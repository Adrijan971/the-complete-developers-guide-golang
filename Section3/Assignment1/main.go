package main

import "fmt"

func main() {

	intSlice := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	for _, e := range intSlice {
		if IsEven(e) == true {
			fmt.Println(e, "Even")
		} else {
			fmt.Println(e, "Odd")
		}
	}
}

//returns true if number is even
func IsEven(i int) bool {
	result := int(i % 2)
	if result == 0 {
		return true
	}
	return false
}
