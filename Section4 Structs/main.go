package main

import "fmt"

type person struct {
    firstName   string
    lastName    string
    contactInfo // same as 'contactInfo contactInfo'
}

type contactInfo struct {
    zipCode int
    email   string
}

func main() {
    jim := person{
        firstName:   "Jim",
        lastName:    "Party",
        contactInfo: contactInfo{zipCode: 94000, email: "jimparty@gmail.com"},
    }
    jim.updateName("Jimmy")
    fmt.Printf("%+v", jim)
}

func (p person) print() {
    fmt.Printf("%+v", p)
}

func (p person) updateName(newFirstName string) {
    p.firstName = newFirstName
}
