package main

import "fmt"

func main() {

    colors := map[string]string{
        "red":   "ff0000",
        "green": "948ff9",
        "blue":  "583g93t",
    }

    colors["red"] = "f0f0f0f0"
    delete(colors, "blue")
    printMap(colors)

}

func printMap(c map[string]string) {
    for key, value := range c {
        fmt.Println("For color", key, "hex color is", value)
    }
}