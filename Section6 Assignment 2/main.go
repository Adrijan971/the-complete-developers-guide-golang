package main

import (
	"fmt"
	"os"
)

func main() {
	path := os.Args[1]
	dat, err := os.ReadFile(path)
	if err != nil {
		fmt.Println("Error is: ", err)
		os.Exit(1)
	}
	fmt.Println(string(dat))
}
