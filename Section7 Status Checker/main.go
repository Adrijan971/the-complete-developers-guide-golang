package main

import (
	"fmt"
	"net/http"
	"time"
)

// This excercise deals with concurency in Go.
// This is also final edit of this excercise, which is a bit complicated because of new
// constructs. At least one revision of this part is needed.

func main() {

	links := []string{
		"http://facebook.com",
		"http://amazon.com",
		"http://google.com",
		"http://golang.org",
		"http://stackoverflow.com",
	}

	c := make(chan string)

	for _, link := range links {
		go checkLink(link, c)
	}

	for l := range c {
		go func(link string) {
			time.Sleep(5 * time.Second)
			checkLink(link, c)
		}(l)
	}
}

func checkLink(link string, c chan string) {
	_, err := http.Get(link)
	if err != nil {
		fmt.Println(link, "Might be down!")
		c <- link
		return
	}
	fmt.Println(link, "is up!")
	c <- link
}
