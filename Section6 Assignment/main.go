package main

import "fmt"

func main() {
	t := triangle{2, 3}
	s := square{5}
	printArea(s)
	printArea(t)
}

type triangle struct {
	height float64
	base   float64
}

func (t triangle) getArea() float64 {
	return 0.5 * t.height * t.base
}

type square struct {
	sideLength float64
}

func (t square) getArea() float64 {
	return t.sideLength * t.sideLength
}

type shape interface {
	getArea() float64
}

// It is important to not that interface var can on be put in parameter section of a method.
func printArea(s shape) {
	fmt.Println(s.getArea())
}
